export function collapseAnimate (element): void {
  const scrollHeight = element.scrollHeight
  const isCollapsed = !element.clientHeight
  const noHeightSet = !element.style.height

  element.style.height = `${(isCollapsed || noHeightSet ? scrollHeight : 0)}px`

  console.log('isCollapsed', isCollapsed)

  if (noHeightSet) return collapseAnimate(element)
}
