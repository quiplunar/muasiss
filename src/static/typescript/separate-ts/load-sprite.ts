export function loadSprite (): void {
  const file = '/assets/sprite/sprite.svg'

  loadFile()

  function loadFile (): void {
    const request = new XMLHttpRequest()
    request.open('GET', file, true)

    request.onload = () => {
      if (request.status >= 200 && request.status < 400) {
        insert(request.responseText)
      }
    }

    request.send()
  }

  function insert (data): void {
    document.body.prepend(hiddenBox(data))
  }

  function hiddenBox (data): HTMLDivElement {
    const hiddenBox = document.createElement('div')
    hiddenBox.style.cssText = `
      height: 0;
      width: 0;
      position: absolute;
      pointer-events: none;
    `
    hiddenBox.insertAdjacentHTML('afterbegin', data)

    return hiddenBox
  }
}
