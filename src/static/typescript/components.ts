import { index } from '@pages/index/index'
import { _template } from '@pages/_template/_template'
import { header } from '@components/header/header'
import { wrapper } from '@components/wrapper/wrapper'
import { logo } from '@components/logo/logo'
import { footer } from '@components/footer/footer'
import { language } from '@components/language/language'
import { icon } from '@components/icon/icon'
import { button } from '@components/button/button'
import { listFeatures } from '@components/list-features/list-features'
import { staticBox } from '@components/static-box/static-box'
import { statics } from '@components/statics/statics'
import { table } from '@components/table/table'
import { listBtc } from '@components/list-btc/list-btc'
import { information } from '@components/information/information'
import { aboutUs } from '@components/about-us/about-us'
import { invest } from '@components/invest/invest'
import { inputDef } from '@components/input-def/input-def'
import { feedback } from '@components/feedback/feedback'
import { team } from '@components/team/team'
import { popup } from '@components/popup/popup'
import { registration } from '@components/registration/registration'
import { inputGrey } from '@components/input-grey/input-grey'
import { recaptcha } from '@components/recaptcha/recaptcha'
import { login } from '@components/login/login'
import { reset } from '@components/reset/reset'
import { inputWhite } from '@components/input-white/input-white'
import { profile } from '@pages/profile/profile'
import { profileHeader } from '@components/profile-header/profile-header'
import { replenishment } from '@components/replenishment/replenishment'
import { wrapInput } from '@components/wrap-input/wrap-input'
import { withdraw } from '@components/withdraw/withdraw'
import { deposit } from '@components/deposit/deposit'
import { greenTable } from '@components/green-table/green-table'
import { referal } from '@components/referal/referal'
import { feedbackFrom } from '@components/feedback-from/feedback-from'
import { account } from '@components/account/account'
import { select } from '@components/select/select'
import { agreement } from '@components/agreement/agreement'
import { hamburger } from '@components/hamburger/hamburger'
import { mobileMenu } from '@components/mobile-menu/mobile-menu'

export function components (): void {
  document.addEventListener('DOMContentLoaded', () => {
    index()
    _template()
    header()
    wrapper()
    logo()
    footer()
    language()
    icon()
    button()
    listFeatures()
    staticBox()
    statics()
    table()
    listBtc()
    information()
    aboutUs()
    invest()
    inputDef()
    feedback()
    team()
    popup()
    registration()
    inputGrey()
    recaptcha()
    login()
    reset()
    inputWhite()
    profile()
    profileHeader()
    replenishment()
    wrapInput()
    withdraw()
    deposit()
    greenTable()
    referal()
    feedbackFrom()
    account()
    select()
    agreement()
    hamburger()
    mobileMenu()
  })
}
