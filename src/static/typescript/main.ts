import { components } from './components'
import { loadSprite } from './separate-ts/load-sprite'

loadSprite()
components()
