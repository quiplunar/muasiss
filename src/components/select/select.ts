export function select (): void {
  if (document.querySelector('.select') === null) return

  function onClick (event: MouseEvent): any {
    const select = (event.target as HTMLElement).closest('.select')
    const item = (event.target as HTMLElement).closest('.select__link')

    if (item !== null) {
      select.querySelector('input').value = item.innerHTML
      return select.classList.remove('select--open')
    }

    if (select !== null) {
      return select.classList.toggle('select--open')
    }

    document.querySelectorAll('.select').forEach((select) => {
      select.classList.remove('select--open')
    })
  }

  document.addEventListener('click', (event) => {
    onClick(event)
  })
}
