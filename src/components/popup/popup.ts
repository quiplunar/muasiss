export function popup (): void {
  function onClick (target: HTMLElement): void {
    if (target.closest('.login__reset') !== null) {
      target.closest('.popup').classList.remove('popup--open')
      document.querySelector('.js-reset-true').classList.add('popup--open')
    }

    if (target.closest('.js-open-pop') !== null) {
      const popup: HTMLElement = document.querySelector(`[data-in="${target.dataset.out}"]`)

      document.body.style.overflow = 'hidden'
      document.body.style.paddingRight = `${getSizeScroll()}px`
      popup.classList.add('popup--open')
    }

    if (target.classList.contains('popup') || target.closest('.agreement__rect') !== null) {
      document.body.style.overflow = ''
      target.closest('.popup').classList.remove('popup--open')
      document.body.style.paddingRight = ''
    }
  }

  function getSizeScroll (): number {
    const div = document.createElement('div')

    div.style.position = 'absolute'
    div.style.pointerEvents = 'none'
    div.style.overflowY = 'scroll'
    div.style.width = '50px'
    div.style.height = '50px'

    document.body.append(div)
    const scrollWidth = div.offsetWidth - div.clientWidth

    div.remove()

    return scrollWidth
  }

  document.addEventListener('click', (event) => {
    onClick((event.target as HTMLElement))
  })
}
