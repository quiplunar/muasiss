export function information (): void {
  if (document.querySelector('.information') === null) return

  const listLine = document.querySelectorAll('.information__line')
  const answerUser = document.querySelector('.js-answer-user')
  const answerSupport = document.querySelector('.information__support-text')

  function onClick (event: MouseEvent): void {
    const line = (event.target as HTMLElement).closest('.information__line')

    if (line !== null) {
      closeAll()
      open(line)
      setValue(line)
    }
  }

  function setValue (line): void {
    answerSupport.innerHTML = line.dataset.answer
    answerUser.innerHTML = line.querySelector('.js-line-user-question').innerHTML
  }

  function open (line): void {
    line.querySelector('.information__plus').innerHTML = '-'
  }

  function closeAll (): void {
    listLine.forEach((elem) => {
      elem.querySelector('.information__plus').innerHTML = '+'
    })
  }

  document.addEventListener('click', (event) => {
    onClick(event)
  })
}
