export function language (): void {
  if (document.querySelector('.language') === null) return

  const hamburger: HTMLElement = document.querySelector('.hamburger')
  const mobileMenu: HTMLElement = document.querySelector('.mobile-menu')

  function onClick (event: MouseEvent): any {
    const target = (event.target as HTMLElement)

    if (target.closest('.js-open-language-panel') !== null) {
      closeMenu()
      return target.closest('.language').classList.toggle('language--close')
    }

    document.querySelector('.language').classList.add('language--close')
  }

  function closeMenu (): void {
    hamburger.classList.remove('is-active')
    mobileMenu.style.height = '0'
  }

  document.addEventListener('click', (event) => {
    onClick(event)
  })
}
