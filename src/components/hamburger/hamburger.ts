import { collapseAnimate } from '../../static/typescript/separate-ts/collapse-animate'

export function hamburger (): void {
  if (document.querySelector('.hamburger') === null) return

  const hamburger = document.querySelector('.hamburger')
  const mobileMenu = document.querySelector('.mobile-menu')

  function onClick (): void {
    collapseAnimate(mobileMenu)
    hamburger.classList.toggle('is-active')
  }

  hamburger.addEventListener('click', () => {
    onClick()
  })
}
