export function profile (): void {
  if (document.querySelector('.profile') === null) return

  const listLink = document.querySelectorAll('.profile__link')
  const listPage = document.querySelectorAll('.profile__page')

  function onClick (event: MouseEvent): void {
    const target = (event.target as HTMLElement).closest('.profile__link')

    if (target !== null && !target.classList.contains('js-exit')) {
      closeAllPage()
      closeAllLink()
      activePageAndLink((target as HTMLElement))
    }
  }

  function activePageAndLink (link: HTMLElement): void {
    const page = document.querySelector(`[data-page="${link.dataset.link}"]`)

    link.classList.add('profile__link--active')
    page.classList.add('profile__page--open')
  }

  function closeAllLink (): void {
    listLink.forEach((link) => {
      link.classList.remove('profile__link--active')
    })
  }

  function closeAllPage (): void {
    listPage.forEach((page) => {
      page.classList.remove('profile__page--open')
    })
  }

  document.addEventListener('click', (event) => {
    onClick(event)
  })
}
