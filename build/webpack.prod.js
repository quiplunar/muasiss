const dev = require('./webpack.dev')
const merge = require('webpack-merge')
const ImageminPlugin = require('imagemin-webpack')
const TerserPlugin = require('terser-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')

module.exports = merge(dev, {
  devtool: 'none',
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          output: {
            comments: false
          }
        },
        extractComments: false
      }),
      new OptimizeCSSAssetsPlugin()
    ]
  },
  plugins: [
    new ImageminPlugin({
      name: '[path][name].[ext]',
      imageminOptions: {
        plugins: [
          ['jpegtran', { progressive: true }],
          ['optipng', { optimizationLevel: 7 }]
        ]
      }
    })
  ]
})
