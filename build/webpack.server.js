const path = require('path')
const dev = require('./webpack.dev')
const merge = require('webpack-merge')

module.exports = merge(dev, {
  devServer: {
    contentBase: path.join(__dirname, '../dist'),
    port: 8080,
    open: true,
    overlay: {
      warnings: true,
      errors: true
    }
  }
})
